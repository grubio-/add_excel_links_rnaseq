#! /usr/bin/perl

use warnings;
use strict;

# Copyright 2016 Ansgar Gruber
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#   http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Overall description: Opens input file, reads database and protein IDs out of the second column of an excel sheet obtained from the RNAseq experiment, 
# prints Microsoft Excel links to ENSEMBL gene pages and JGI protein pages (if existing) into first output file, adds two columns with links to table and
# prints table to second output file, start with input filename as option 'add_excel_links_RNAseq.pl input' output file will be named 'input_excel_linked'.

# open file with protein IDs:
my $input_fn = "\$input_fn_initial"
  ; # file with summary table (IDs in first column)
if ( scalar(@ARGV) == 0 ) {
    print(
"\nPlease type the name of the text file with the content of the summary Excel sheet (text file, specify name with file extension).\n"
    );
    $input_fn = <stdin>;
    chomp($input_fn);
    linebreak_remove($input_fn);    # remove remaining linebreaks
}
elsif ( scalar(@ARGV) == 1 ) {
    $input_fn = $ARGV[0];
    chomp($input_fn);
    linebreak_remove($input_fn);
}
else {
    print(
"\nFailed to identify input filename for this run of add_excel_links_RNAseq.pl\n, usage 'perl add_excel_links_RNAseq.pl \$input_fn (text file, specify name with file extension), if run from Windows cmd, append '>log.txt 2>&1' to redirect output into 'log.txt'.\n"
    );
}
open( my $input_fh, "<", $input_fn )
  or die( "add_excel_links_RNAseq.pl died: unable to open " . $input_fn . ".\n" );

# Read through file and generate output file

open( my $output1_fh, ">", $input_fn."_excel_links" )
  or die( "add_excel_links_RNAseq.pl died: unable to open " . $input_fn . "_links" . ".\n" );
open( my $output2_fh, ">", $input_fn."_links_added" )
  or die( "add_excel_links_RNAseq.pl died: unable to open " . $input_fn . "_links_added" . ".\n" );

my $firstline = <$input_fh>;
chomp($firstline);
    linebreak_remove($firstline);       # remove remaining linebreaks if present
my $s="\t"; # field separator
my @firstline=split($s, $firstline, -1); # splitted content of each input line, caution: split /PATTERN/,EXPR,LIMIT, only if LIMIT is specified as negative, empty fields at end of line will be retained
splice(@firstline, 1, 0, "ENSEMBL link", "JGI link");
    print $output2_fh (join( $s, @firstline)."\n");
    print $output1_fh ("\n");

while ( my $line = <$input_fh> ) {
    chomp($line);
    linebreak_remove($line);       # remove remaining linebreaks if present
    my @line=split($s, $line, -1); # splitted content of each input line, caution: split /PATTERN/,EXPR,LIMIT, only if LIMIT is specified as negative, empty fields at end of line will be retained
    my $link_E = "";
    my $link_J = "";
    unless($line[0] eq ""){
        my @description=split("\\|", $line[0]);
        $link_E="=HYPERLINK(\"http://protists.ensembl.org/Phaeodactylum_tricornutum/Gene/Summary?db=core;t=".$description[0]."\", \"".$description[0]."\")"; # generate ENSEMBL link
        my @id_strip=split("_", $description[0]);
        my @id_stripped=split("\\.", $id_strip[1]);
        my $dbv_J="JGI db version";
        my $id_J="initial JGI id";
        if (substr($id_stripped[0],0,6) eq "Jdraft") {
            $dbv_J="Phatr2_bd";
            $id_J=substr($id_stripped[0],6);
            $link_J = "=HYPERLINK(\"http://genome.jgi-psf.org/cgi-bin/dispGeneModel?db=".$dbv_J."&id=".$id_J."\", \"".$id_J."\")"; # generate JGI link
        }
        elsif (substr($id_stripped[0],0,6) eq "draftJ") {
            $dbv_J="Phatr2_bd";
            $id_J=substr($id_stripped[0],6);
            $link_J = "=HYPERLINK(\"http://genome.jgi-psf.org/cgi-bin/dispGeneModel?db=".$dbv_J."&id=".$id_J."\", \"".$id_J."\")"; # generate JGI link
        }
        elsif (substr($id_stripped[0],0,1) eq "J") {
            $dbv_J="Phatr2";
            $id_J=substr($id_stripped[0],1);
            $link_J = "=HYPERLINK(\"http://genome.jgi-psf.org/cgi-bin/dispGeneModel?db=".$dbv_J."&id=".$id_J."\", \"".$id_J."\")"; # generate JGI link
        }
        else{
            $dbv_J="";
            $id_J="";
            $link_J="n.a.";
        }
    }
    splice(@line, 1, 0, $link_E, $link_J);
    print $output1_fh ($link_E."\t".$link_J."\n");
    print $output2_fh (join( $s, @line)."\n");
}
close($input_fh);
close($output1_fh);
close($output2_fh);

# final print statement
print(
"\nThe Excel links have been printed to file '".$input_fn."_excel_linked'.\nThe complete table with the links added to the last column has been printed to file '".$input_fn."_links_added'.\n\nThank you for using add_excel_links_RNAseq.pl, please contact ansgar.gruber\@uni-konstanz.de with any question concerning the program :-\)\n"
);

# Subroutines

sub linebreak_remove
{ # removes linebreaks from the end of the scalar value passed to it
    unless ( scalar(@_) == 1 ) {
        die(
"Subroutine linebreak_remove needs exactly one element of \@\_ (from wich any linebreaks that might occur at the end will be removed), program died, please check.\n"
        );
    }
    else {
        $_[0] =~ s/\R*$//g;
        return ( $_[0] );
    }
}    # end of sub linebreak_remove